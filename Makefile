# Copyright 1998 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for Generic fetcher module
#

COMPONENT   = Generic
RESFSDIR    = ${RESDIR}${SEP}URL${SEP}${TARGET}
OBJS        = commands connect headers protocol readdata \
              ses_ctrl start status stop utils writedata module
CMHGDEPENDS = module
CDEFINES    = -D${SYSTEM}
CINCLUDES   = ${TCPIPINC}
ROMCDEFINES = -DROM
CFLAGS      = ${C_NOWARN_NON_ANSI_INCLUDES}
CDFLAGS    += -DTRACE -DDEBUGLIB
HDRS        =
LIBS       += ${FETCHLIB} ${NET5LIBS}

include CModule

# Dynamic dependencies:
